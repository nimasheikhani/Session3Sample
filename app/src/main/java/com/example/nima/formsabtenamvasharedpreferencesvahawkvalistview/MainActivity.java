package com.example.nima.formsabtenamvasharedpreferencesvahawkvalistview;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.nima.formsabtenamvasharedpreferencesvahawkvalistview.Utils.PublicMethods;
import com.orhanobut.hawk.Hawk;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    EditText txtName;
    EditText txtLastName;
    EditText txtEmail;
    EditText txtPassword;
    Button btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtName = (EditText) findViewById(R.id.txtName);
        txtLastName = (EditText) findViewById(R.id.txtLastName);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnSave) {
            PublicMethods.SetShared(mContext, "Name", txtName.getText().toString());
            PublicMethods.SetShared(mContext, "LastName", txtLastName.getText().toString());
            PublicMethods.SetShared(mContext, "Email", txtEmail.getText().toString());

            Hawk.put("Password", txtPassword.getText().toString());

            ClearText();

            Toast.makeText(mContext, R.string.success_save_registration_form, Toast.LENGTH_LONG).show();
        }
    }

    private void ClearText() {
        txtName.setText("");
        txtLastName.setText("");
        txtEmail.setText("");
        txtPassword.setText("");
    }
}
