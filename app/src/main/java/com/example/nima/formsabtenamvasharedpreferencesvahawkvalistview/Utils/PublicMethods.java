package com.example.nima.formsabtenamvasharedpreferencesvahawkvalistview.Utils;

import android.content.Context;
import android.preference.PreferenceManager;
import android.widget.Toast;

/**
 * Created by Nima on 12/2/2017.
 */

public class PublicMethods {
    public static void SetShared(Context mContext, String key, String value) {
        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString(key, value).apply();
    }

    public static String getShared(Context mContext, String key, String defaultValue) {
        return PreferenceManager.getDefaultSharedPreferences(mContext).getString(key, defaultValue);
    }

    public static  void ShowToast(Context mContext, String message)
    {
        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
    }
}
