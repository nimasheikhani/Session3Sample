package com.example.nima.formsabtenamvasharedpreferencesvahawkvalistview.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nima.formsabtenamvasharedpreferencesvahawkvalistview.Models.MailModel;
import com.example.nima.formsabtenamvasharedpreferencesvahawkvalistview.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Nima on 12/3/2017.
 */

public class EmailAdapter extends BaseAdapter {

    Context mContext;
    List<MailModel> Emails;

    public EmailAdapter(Context mContext, List<MailModel> emails) {
        this.mContext = mContext;
        Emails = emails;
    }

    @Override
    public int getCount() {
        return Emails.size();
    }

    @Override
    public Object getItem(int position) {
        return Emails.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View rowView = LayoutInflater.from(mContext).inflate(R.layout.email_item, viewGroup, false);

        MailModel mail = Emails.get(position);

        ImageView emailImage = (ImageView) rowView.findViewById(R.id.mailImage);
        ImageView emailAttachments = (ImageView) rowView.findViewById(R.id.imgattachment);
        TextView emailSubject = (TextView) rowView.findViewById(R.id.txtEmailSubject);
        TextView emailBody = (TextView) rowView.findViewById(R.id.txtEmailBody);

        Picasso.with(mContext).load(mail.getImageUrl()).into(emailImage);
        if (mail.getHasAttachment() == false) {
            emailAttachments.setVisibility(View.GONE);
        }
        emailSubject.setText(mail.getSubject());
        emailBody.setText(mail.getBody());

        return rowView;
    }

    public void MarkAsRead(View view) {
        view.setBackgroundColor(Color.parseColor("#FFFBF6D0"));
        TextView txtEmailSubject = (TextView) view.findViewById(R.id.txtEmailSubject);
        txtEmailSubject.setTypeface(null, Typeface.NORMAL);
        txtEmailSubject.setTypeface(null, Typeface.ITALIC);
    }
}
