package com.example.nima.formsabtenamvasharedpreferencesvahawkvalistview;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.nima.formsabtenamvasharedpreferencesvahawkvalistview.Adapters.EmailAdapter;
import com.example.nima.formsabtenamvasharedpreferencesvahawkvalistview.Models.MailModel;
import com.example.nima.formsabtenamvasharedpreferencesvahawkvalistview.Utils.PublicMethods;

import java.util.ArrayList;
import java.util.List;

public class ListViewActivity extends BaseActivity {

    ListView ListViewEmails;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        List<MailModel> EmailList = CreateEmailList();

        final EmailAdapter adapter = new EmailAdapter(mContext, EmailList);

        ListViewEmails = (ListView) findViewById(R.id.EmailList);
        ListViewEmails.setAdapter(adapter);
        ListViewEmails.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                MailModel ClickedEmail = ((MailModel) adapterView.getItemAtPosition(position));
                PublicMethods.ShowToast(mContext,ClickedEmail.getSubject() + " CLICKED!!!! ");
                adapter.MarkAsRead(view);
            }
        });
    }

    private List<MailModel> CreateEmailList() {
        List<MailModel> EmailList = new ArrayList<>();

        EmailList.add(new MailModel("http://dryicons.com/uploads/icon/preview/3368/small_1x_business_user_accept.png", "Subject 1", "Some Body 1...", true));
        EmailList.add(new MailModel("http://dryicons.com/uploads/icon/preview/3349/small_1x_black_business_user.png", "Subject 2", "Some Body 2...", true));
        EmailList.add(new MailModel("http://dryicons.com/uploads/icon/preview/3370/small_1x_business_user_comment.png", "Subject 3", "Some Body 3...", false));
        EmailList.add(new MailModel("http://dryicons.com/uploads/icon/preview/3386/small_1x_business_user_search.png", "Subject 4", "Some Body 4...", false));
        EmailList.add(new MailModel("http://dryicons.com/uploads/icon/preview/7780/small_1x_5aa79401-127f-48ab-8074-14dbb07a8c43.png", "Subject 5", "Some Body 5...", false));
        EmailList.add(new MailModel("http://dryicons.com/uploads/icon/preview/1041/small_1x_italy.png", "Subject 6", "Some Body 6...", false));
        EmailList.add(new MailModel("http://dryicons.com/uploads/icon/preview/3879/icon_grid_1x_patient_chart.png", "Subject 7", "Some Body 7...", true));
        EmailList.add(new MailModel("http://dryicons.com/uploads/icon/preview/9614/icon_grid_1x_40af202e-3408-4a10-9c51-82f4e9f2257e.png", "Subject 8", "Some Body 8...", false));

        return  EmailList;
    }
}
