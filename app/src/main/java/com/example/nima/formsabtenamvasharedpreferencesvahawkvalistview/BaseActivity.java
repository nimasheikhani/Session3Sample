package com.example.nima.formsabtenamvasharedpreferencesvahawkvalistview;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.orhanobut.hawk.Hawk;

/**
 * Created by Nima on 12/2/2017.
 */

public class BaseActivity extends AppCompatActivity {
    public Context mContext = this;
    public Activity mAvtivity= this;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Hawk.init(mContext).build();
    }
}
