package com.example.nima.formsabtenamvasharedpreferencesvahawkvalistview.Models;

/**
 * Created by Nima on 12/3/2017.
 */

public class MailModel {
    String ImageUrl;
    String Subject;
    String Body;
    Boolean HasAttachment;

    public MailModel(String imageUrl, String subject, String body, Boolean hasAttachment) {
        ImageUrl = imageUrl;
        Subject = subject;
        Body = body;
        HasAttachment = hasAttachment;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    public String getBody() {
        return Body;
    }

    public void setBody(String body) {
        Body = body;
    }

    public Boolean getHasAttachment() {
        return HasAttachment;
    }

    public void setHasAttachment(Boolean hasAttachment) {
        HasAttachment = hasAttachment;
    }
}
